# Batch Connect - RStudio Server

This app uses Apptainer containers to run a self-contained RStudio Server instance.

## Known Issues

- To avoid conflicts with other installations of R it is reccommended to change the default R User Library location.
  - This will need to be done in your container build as this app doesn't currently modify that value.
- The 'rserver' executable needs to be available from the "$PATH".  If not the app will fail to launch.
  - This should be handled in the container build recipe.
- Using this app with the Apptainer '--userns' flag or in sandbox can have issues on GPFS filesystems.
  - We have seen apptainer fail or take an extremely long time to convert from sif to sandbox when running on a heavily congested GPFS file system. GPFS itself also doesn't support subuid/subgid which breaks sandbox.
- Singularity has a really nasty bug on RHEL 7, and other systems running a similar kernel (3.x), that causes compute nodes to become unresponsive and eventually completely lock up [GitHub Issue](https://github.com/apptainer/singularity/issues/5850#issuecomment-1029918212).
  - This has been marked as will not fix by both Singularity and RHEL 7.  It is recommended to upgrade to RHEL 8 where the issue is fixed in the Kernel.
  - For those that have to stay on RHEL 7 there is a temporary workaround made by [pja237](https://github.com/pja237/kp_oom)

## Site Customizations

The following are site specific settings that will need review should this repo be forked.

- `form.js`
  - There is a function here which sets dynamic max form field values for specific partitions.
  - A default partition is set on form load.
- `form.yml.erb`
  - A dynamic list of partitions is generated from a call to slurm.
  - Default Min/Max values for form fields.
  - Cluster name.
  - Links to documentation.
  - submission type/environment entries.
- `partitions.json`
  - Partition values for dynamic partition list in form.yml.erb -> form 'custom_queue'.
- `manifest.yml`
  - links to documentation.
- `submit.yml.erb`
  - Adjujst parameters to match the scheduler/settings at your site.
- `template/before.sh`
  - Host/host value.
- `tempalte/script.sh`
  - Calls to apptainer assumes that the nodes apptainer.conf will specify any cluster specific mounts such as home, scratch, and project directories.

## Example Singularity Build File

rstudio-server.def (~1.5GB)

```markdown
BootStrap: docker
From: quay.io/centos/centos:stream8

%runscript
exec "$@"

%environment
export TZ="America/Denver"
export LANG="en_US.UTF-8"
export LC_COLLATE="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
export LC_MESSAGES="en_US.UTF-8"
export LC_MONETARY="en_US.UTF-8"
export LC_NUMERIC="en_US.UTF-8"
export LC_TIME="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"
export CPATH=$CPATH:/usr/include/openmpi-x86_64
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig
export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
export PATH=/usr/lib/rstudio-server/bin:${PATH}

%post
# Setup Environment and Fix Locale
dnf install -y langpacks-en glibc-langpack-en glibc-locale-source glibc-common
localedef --quiet -v -c -i en_US -f UTF-8 en_US.UTF-8 || if [ $? -ne 1 ]; then exit $?; fi
export TZ="America/Denver"
export CPATH=$CPATH:/usr/include/openmpi-x86_64
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig
export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH

###########################
# Start: Default Packages #
###########################

# Install EPEL
dnf install -y epel-release

# Enable Additional Built-In Repos
dnf config-manager --set-enabled powertools

# Upgrade Packages
dnf upgrade -y

# Install Dev Tools
dnf groupinstall -y --with-optional "Development Tools" "Scientific Support"
dnf install -y wget ca-certificates mariadb-connector-c-devel curl-devel openssl-devel llvm llvm-devel llvm-static llvm-toolset libxml2-devel
echo "/usr/lib64/openmpi/lib" > /etc/ld.so.conf.d/openmpi.conf
ldconfig

# Create empty file for nvidia-smi (fixes underlay warning at runtime)
function placeholder(){
	cat <<-EOF > "$1"
		#!/bin/bash
		echo "$1 placeholder for underlay"
	EOF
	chmod +rx "$1"
}
placeholder /usr/bin/nvidia-smi
placeholder /usr/bin/nvidia-persistenced
placeholder /usr/bin/nvidia-debugdump
placeholder /usr/bin/nvidia-cuda-mps-server
placeholder /usr/bin/nvidia-cuda-mps-control

# Install R (https://docs.rstudio.com/resources/install-r/)
R_VERSION=4.1.2
curl -O https://cdn.rstudio.com/r/centos-8/pkgs/R-${R_VERSION}-1-1.x86_64.rpm
dnf install -y R-${R_VERSION}-1-1.x86_64.rpm
rm -f R-${R_VERSION}-1-1.x86_64.rpm
ln -s /opt/R/${R_VERSION}/bin/R /usr/local/bin/R
ln -s /opt/R/${R_VERSION}/bin/Rscript /usr/local/bin/Rscript

# Add a default CRAN mirror
echo "options(repos = c(CRAN = 'https://cran.rstudio.com/'), download.file.method = 'libcurl')" >> /opt/R/${R_VERSION}/lib/R/etc/Rprofile.site

# Add Timezone to R Site Environment File
echo "TZ='$TZ'" >> /opt/R/${R_VERSION}/lib/R/etc/Renviron

# Adjust Platform and Libs (update on R or OS version change!!)
sed -i '/^R_PLATFORM=/ c\R_PLATFORM=${R_PLATFORM-"el8-x86_64-singularity"}' /opt/R/${R_VERSION}/lib/R/etc/Renviron
sed -i '/^R_LIBS_USER=/ c\R_LIBS_USER=${R_LIBS_USER-"~/R/el8-x86_64-singularity-library/4.1"}' /opt/R/${R_VERSION}/lib/R/etc/Renviron

# Install R devtools
R -e "install.packages('devtools')"

# Install R Studio Server
RSTUDIO_VERSION=2021.09.2-382
curl -O https://download2.rstudio.org/server/centos8/x86_64/rstudio-server-rhel-${RSTUDIO_VERSION}-x86_64.rpm
dnf install -y rstudio-server-rhel-${RSTUDIO_VERSION}-x86_64.rpm
rm -f rstudio-server-rhel-${RSTUDIO_VERSION}-x86_64.rpm

#########################
# End: Default Packages #
#########################

########################
# Start: User Packages #
########################

# https://clint.id.au/?p=1428
dnf module enable -y nodejs:16
dnf install -y wget nodejs nodejs-devel npm openblas java-1.8.0-openjdk-devel zlib-devel libicu-devel libpng-devel libcurl-devel libxml2-devel openssl-devel openmpi-devel python3-numpy python3-matplotlib netcdf4-python3 netcdf-devel netcdf python3-pandas python3-basemap proj-devel gdal-devel monitorix gnuplot ImageMagick librsvg2-devel libsodium-devel libwebp-devel cairo-devel hunspell-devel openssl-devel poppler-cpp-devel protobuf-devel mariadb-devel redland-devel cyrus-sasl-devel libtiff-devel tcl-devel tk-devel xauth mesa-libGLU-devel glpk-devel libXt-devel gsl-devel fftw-devel bzip2-devel geos-devel gtk2-devel gtk3-devel libjpeg-turbo-devel blas-devel lapack-devel mpfr-devel unixODBC-devel libsndfile-devel udunits2-devel postgresql-devel libRmath-devel qt5-devel libdb-devel octave-devel hiredis-devel poppler-glib-devel boost-devel czmq-devel ImageMagick-c++-devel file-devel opencl-headers sqlite-devel

# Install tidyverse
R -e "install.packages('tidyverse')"

# Install V8
echo "DOWNLOAD_STATIC_LIBV8='1'" >> /opt/R/${R_VERSION}/lib/R/etc/Renviron
R -e "Sys.setenv(DOWNLOAD_STATIC_LIBV8=1);install.packages('V8')"

######################
# End: User Packages #
######################

# cleanup dnf
dnf clean all
rm -rf /var/cache/dnf/*

%test
command -v R &>/dev/null || exit 1
command -v rstudio-server &>/dev/null || exit 2
command -v rserver &>/dev/null || exit 3
```
